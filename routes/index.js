var express = require('express');
var router = express.Router();

var path = require('path');
var debug = require('debug')('sep2:server');
var Firebase = require("firebase");

var ref = new Firebase("https://colombo.firebaseio.com/");
var authData = ref.getAuth();

/* Check for authentication state before routing */
router.use(function(req, res, next) {
	authData = ref.getAuth();

	if (req.path == '/') return next();
	if (req.path == '/login') return next();

	if (authData) {
		debug("User " + authData.uid + " is logged in with " + authData.provider);
		if (authData.password.email) {
			next();
		} else {
			debug('User is logged in with different provider!');
			res.redirect('logout');
			//next();
		}

	} else {
		debug("User is not logged in ONE");
		res.sendFile(path.join(__dirname + '/../views/login.html'));
		//next();
	}

});

/* GET landing page page. */
router.get('/', function(req, res, next) {
	res.redirect('login');
});

/* GET home page */
router.get('/dashboard', function(req, res, next) {
	//var authData = ref.getAuth();


	ref.child("profile").once("value", function(snapshot) {
		var userCount = snapshot.numChildren();
		debug("Number of users: ", userCount);

		res.render('index.pug', {
			username: authData.password.email,
			profileImage: authData.password.profileImageURL,
			noOfUsers: userCount,
			active: 'dashboard'
		});

		/*
				if (authData) {
					debug("User " + authData.uid + " is logged in with " + authData.provider);
					if (authData.password.email) {
						res.render('index.pug', {
							username: authData.password.email,
							profileImage: authData.password.profileImageURL,
							noOfUsers: userCount
						});
					} else {
						debug('ELsE');
						res.render('index.pug');
					}
				} else {
					debug("User is not logged in");
					res.render('index.pug');
				}
		*/
	});
});

/* GET calender page */
router.get('/calender', function(req, res, next) {
	//var authData = ref.getAuth();


	ref.child("profile").once("value", function(snapshot) {
		var userCount = snapshot.numChildren();
		debug("Number of users: ", userCount);

		res.render('calender.pug', {
			username: authData.password.email,
			profileImage: authData.password.profileImageURL,
			noOfUsers: userCount,
			active: 'calender'
		});

		/*
				if (authData) {
					debug("User " + authData.uid + " is logged in with " + authData.provider);
					if (authData.password.email) {
						res.render('index.pug', {
							username: authData.password.email,
							profileImage: authData.password.profileImageURL,
							noOfUsers: userCount
						});
					} else {
						debug('ELsE');
						res.render('index.pug');
					}
				} else {
					debug("User is not logged in");
					res.render('index.pug');
				}
		*/
	});
});

/* GET login page */
router.get('/login', function(req, res, next) {
	//res.sendFile(path.join(__dirname + '/../views/login.html'));
	res.render('login.pug');
});

/* POST login page */
router.post('/login', function(req, res, next) {

	debug('Login info recieved');

	ref.authWithPassword({
		email: req.body.email,
		password: req.body.password
	}, function(error, userData) {
		if (error) {
			var message;
			switch (error.code) {
				case "NETWORK_ERROR":
					message = "No internet connection.";
					console.log("No internet connection.");
					break;
				case "INVALID_EMAIL":
					message = "The specified user account email is invalid.";
					console.log("The specified user account email is invalid.");
					break;
				case "INVALID_PASSWORD":
					message = "The specified user account password is incorrect.";
					console.log("The specified user account password is incorrect.");
					break;
				case "INVALID_USER":
					message = "The specified user account does not exist.";
					console.log("The specified user account does not exist.");
					break;
				default:
					message = "Error logging user in.";
					console.log("Error logging user in:", error);
			}
			debug('Cannot login:', message);
			res.render('login.pug', {'message':message, 'error':error});
		} else {
			debug("Successfully logged in user account with uid:", userData.uid);
			res.redirect('dashboard');
		}
	}, {
		remember: 'sessionOnly'
	});
});

/* GET logout request */
router.get('/logout', function(req, res, next) {
	if (authData) {
		debug('Logging out user..');
		ref.unauth();
	} else {
		debug("User is not logged in");
	}
	res.redirect('/login');
});

/* GET registration page */
router.get('/register', function(req, res, next) {
	//res.sendFile(path.join(__dirname + '/../views/register.html'));
	res.render('register.pug');
});

/* POST registration page */
router.post('/register', function(req, res, next) {
	debug('Registration info received');

	ref.createUser({
		email: req.body.email,
		password: req.body.password
	}, function(error, userData) {
		if (error) {
			debug("Error creating user:", error);
			res.redirect('register');
		} else {
			debug("Successfully created admin account with uid:", userData.uid);
			res.redirect('register');
		}
	});
});

/* Admin-LTE template files */
/* GET index page 1 */
router.get('/index.html', function(req, res, next) {
	res.sendFile(path.join(__dirname + '/../views/index.html'));
});

/* GET index page 2 */
router.get('/index2.html', function(req, res, next) {
	res.sendFile(path.join(__dirname + '/../views/index2.html'));
});

/* GET starter page */
router.get('/starter.html', function(req, res, next) {
	res.sendFile(path.join(__dirname + '/../views/starter.html'));
});
/* /Admin-LTE template files */

/* GET layout page */
router.get('/layout', function(req, res, next) {
	res.render('layout.pug');
});

module.exports = router;