gapi.analytics.ready(function() {

  /* Constant view ID */
  var IDS = 'ga:130325880';

  // Authorize the user.
  /* Constant Client ID*/
  var CLIENT_ID = '856563878469-epfuvtm7ue5cflhbrjmsiikuubnlak1p.apps.googleusercontent.com';

  gapi.analytics.auth.authorize({
    container: 'auth-button',
    clientid: CLIENT_ID,
  });

  /**
   * Extend the Embed APIs `gapi.analytics.report.Data` component to
   * return a promise the is fulfilled with the value returned by the API.
   * @param {Object} params The request parameters.
   * @return {Promise} A promise.
   */
  function query(params) {
    return new Promise(function(resolve, reject) {
      var data = new gapi.analytics.report.Data({
        query: params
      });
      data.once('success', function(response) {
          resolve(response);
        })
        .once('error', function(response) {
          reject(response);
        })
        .execute();
    });
  }

  /* Comments */
  query({
    'ids': IDS,
    'metrics': 'ga:eventValue',
    'dimensions': 'ga:eventCategory',
    'filter': 'ga:eventCategory==Comments'
  }).then(function(results) {
    console.log('comments-span: ', results.rows[0][1]);
    $('#comments-span').text(results.rows[0][1]);
  }).catch(function(error) {
    console.log("Error:", error);
  });

  /* Likes */
  query({
    'ids': IDS,
    'metrics': 'ga:eventValue',
    'dimensions': 'ga:eventCategory',
    'filter': 'ga:eventCategory==Likes'
  }).then(function(results) {
    console.log('likes-span: ', results.rows[0][1]);
    $('#likes-span').text(results.rows[0][1]);
  }).catch(function(error) {
    console.log("Error:", error);
  });

  /* Direct Messages */
  query({
    'ids': IDS,
    'metrics': 'ga:eventValue',
    'dimensions': 'ga:eventCategory',
    'filter': 'ga:eventCategory==Direct Messages'
  }).then(function(results) {
    console.log('dm-span: ', results.rows[0][1]);
    $('#dm-span').text(results.rows[0][1]);
  }).catch(function(error) {
    console.log("Error:", error);
  });

  var report = new gapi.analytics.report.Data({
    query: {
      ids: IDS,
      metrics: 'ga:sessions',
      dimensions: 'ga:date'
    }
  });

  report.on('success', function(response) {

  });

  report.on('error', function(response) {
    console.error('hah', response.error.message);
  });

  report.execute();

  var report2 = new gapi.analytics.report.Data({
    query: {
      ids: IDS,
      metrics: 'ga:avgSessionDuration'
    }
  });

  report2.on('success', function(response) {
    var secondsRaw = response.rows[0];
    var hours = Math.floor(secondsRaw / 3600);
    secondsRaw %= 3600;
    var minutes = Math.floor(secondsRaw / 60);
    var seconds = Math.floor(secondsRaw % 60);

    $('#avg-session-dur').text(hours + " hours " + minutes + " minutes " + seconds + " seconds ");
  });

  report2.on('error', function(response) {
    console.error(response.error.message);
  });

  report2.execute();

  var report3 = new gapi.analytics.report.Data({
    query: {
      ids: IDS,
      dimensions: 'ga:latitude, ga:longitude, ga:country',
      metrics: 'ga:sessions',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
    }
  });

  report3.on('success', function(response) {

    var data = [];

    response.rows.forEach(function(item) {
      data.push({
        latLng: [item[0], item[1]],
        name: item[2] + ": " + item[3]
      });
    });
    $('#world-map-markers').vectorMap({
      map: 'world_mill_en',
      normalizeFunction: 'polynomial',
      hoverOpacity: 0.7,
      hoverColor: false,
      backgroundColor: 'transparent',
      regionStyle: {
        initial: {
          fill: 'rgba(210, 214, 222, 1)',
          "fill-opacity": 1,
          stroke: 'none',
          "stroke-width": 0,
          "stroke-opacity": 1
        },
        hover: {
          "fill-opacity": 0.7,
          cursor: 'pointer'
        },
        selected: {
          fill: 'yellow'
        },
        selectedHover: {}
      },
      markerStyle: {
        initial: {
          fill: '#00a65a',
          stroke: '#111'
        }
      },
      markers: data
    });
  });

  report3.on('error', function(response) {
    console.error(response.error.message);
  });

  report3.execute();

  //-----------------------
  //- MONTHLY SALES CHART -
  //-----------------------
  function renderChart() {
    // Get context with jQuery - using jQuery's .get() method.
    var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
    // This will get the first returned node in the jQuery collection.
    var salesChart = new Chart(salesChartCanvas);

    var thisWeek = query({
      'ids': IDS,
      'dimensions': 'ga:date,ga:nthDay',
      'metrics': 'ga:sessions',
      'start-date': '7daysAgo',
      'end-date': 'yesterday',
      'max-results': 7
    });

    var lastWeek = query({
      'ids': IDS,
      'dimensions': 'ga:date,ga:nthDay',
      'metrics': 'ga:sessions',
      'start-date': '14daysAgo',
      'end-date': '7daysAgo',
      'max-results': 7
    });

    Promise.all([thisWeek, lastWeek]).then(function(results) {

      var data1 = results[0].rows.map(function(row) {
        return +row[2];
      });
      var data2 = results[1].rows.map(function(row) {
        return +row[2];
      });
      var labels = results[1].rows.map(function(row) {
        return +row[0];
      });

      labels = labels.map(function(label) {
        return label;
      });

      labels = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
      var salesChartData = {
        labels: labels,
        datasets: [{
          label: 'Last Week',
          fillColor: 'rgba(220,220,220,0.5)',
          strokeColor: 'rgba(220,220,220,1)',
          data: data2
        }, {
          label: 'This Week',
          fillColor: 'rgba(151,187,205,0.5)',
          strokeColor: 'rgba(151,187,205,1)',
          data: data1
        }]
      };
      var salesChartOptions = {
        //Boolean - If we should show the scale at all
        showScale: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: false,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - Whether the line is curved between points
        bezierCurve: true,
        //Number - Tension of the bezier curve between points
        bezierCurveTension: 0.3,
        //Boolean - Whether to show a dot for each point
        pointDot: false,
        //Number - Radius of each point dot in pixels
        pointDotRadius: 4,
        //Number - Pixel width of point dot stroke
        pointDotStrokeWidth: 1,
        //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
        pointHitDetectionRadius: 20,
        //Boolean - Whether to show a stroke for datasets
        datasetStroke: true,
        //Number - Pixel width of dataset stroke
        datasetStrokeWidth: 2,
        //Boolean - Whether to fill the dataset with a color
        datasetFill: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
        //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true
      };

      //Create the line chart
      salesChart.Line(salesChartData, salesChartOptions);
    });
    /*    var salesChartData = {
          labels: ["January", "February", "March", "April", "May", "June", "July"],
          datasets: [{
            label: "Posts",
            fillColor: "rgb(210, 214, 222)",
            strokeColor: "rgb(210, 214, 222)",
            pointColor: "rgb(210, 214, 222)",
            pointStrokeColor: "#c1c7d1",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgb(220,220,220)",
            data: [65, 59, 80, 81, 56, 55, 40]
          }, {
            label: "Comments",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgba(60,141,188,0.8)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: [28, 48, 40, 19, 86, 27, 90]
          }]
        };
    */
  }
  renderChart();
  //---------------------------
  //- END MONTHLY SALES CHART -
  //---------------------------

  /**
   * Create a new ActiveUsers instance to be rendered inside of an
   * element with the id "active-users-container" and poll for changes every
   * five seconds.
   */
  var activeUsers = new gapi.analytics.ext.ActiveUsers({
    ids: IDS,
    container: 'active-users-container',
    pollingInterval: 5
  });

  /**
   * Add CSS animation to visually show the when users come and go.
   */
  activeUsers.once('success', function() {
    var element = this.container.firstChild;
    var timeout;

    this.on('change', function(data) {
      var element = this.container.firstChild;
      var animationClass = data.delta > 0 ? 'is-increasing' : 'is-decreasing';
      element.className += (' ' + animationClass);

      clearTimeout(timeout);
      timeout = setTimeout(function() {
        element.className =
          element.className.replace(/ is-(increasing|decreasing)/g, '');
      }, 3000);
    });
  });

  // Create the view selector.

  var viewSelector = new gapi.analytics.ViewSelector({
    container: 'view-selector'
  });

  // Create the timeline chart.

  var timeline = new gapi.analytics.googleCharts.DataChart({
    reportType: 'ga',
    query: {
      'dimensions': 'ga:date',
      'metrics': 'ga:sessions',
      'start-date': '30daysAgo',
      'end-date': 'yesterday',
    },
    chart: {
      type: 'LINE',
      container: 'timeline',
      options: {
        fontSize: 12,
        width: '100%',
        height: '100%',
        legend: {
          position: 'bottom'
        }
      }
    }
  });

  activeUsers.execute();
  // Hook up the components to work together.

  gapi.analytics.auth.on('success', function(response) {
    viewSelector.execute();
  });

  viewSelector.on('change', function(ids) {
    var newIds = {
      query: {
        ids: ids
      }
    };
    //timeline.set(newIds).execute();
    /*activeUsers.set(IDS).execute();*/
  });
});